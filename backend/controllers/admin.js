const Anime = require('../models/Anime');

exports.getIndex = async (req, res) => {
    const anime = await Anime.find((data) => data);

    try {
        // console.log(anime);
        res.json(anime);
    } catch (error) {
        console.log(error);
    }
};

exports.getAnime = async (req, res) => {a
    const animeId = req.params.animeId;

    const anime = await Anime.findById(animeId, (anime) => anime);

    try {
        console.log(anime);
        res.status(200).render('anime', { anime: anime });
    } catch (error) {
        console.log(error);
    }
};

