const mongoose = require('mongoose');


const AnimeSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('actors', AnimeSchema);


