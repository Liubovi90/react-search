import React from 'react';

const SearchDetail = (props) => {
    const { name } = props;
    return (
      <>
        <p>{name}</p>
      </>
    );
  };

  export default SearchDetail;