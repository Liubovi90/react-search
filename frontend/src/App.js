import React, { Fragment, useEffect, useState } from 'react';
import styled from '@emotion/styled'
import SearchDetail from './components/SearchDetail'


const App = () => {

  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);   
  const [search, setSearch] = useState("");
  const [filteredActors, setFilteredActors] = useState([]);

    useEffect(() => {
       setLoading(true);
        const getAPI = async () => {
            const response = await fetch('http://localhost:8080/');
            const data = await response.json();
            try {         
                console.log(data);         
                setItems(data);
                setFilteredActors(data);
                setLoading(false);
            } catch (error) {
                console.log(error);
            }
        };
        getAPI();
    }, []);

  // Filter
    useEffect(() => {
      setFilteredActors(
        items.filter((actor) =>
        actor.name.toLowerCase().includes(search.toLowerCase())
        )
      );
    }, [search, items]);
  
    //Loading
    if (loading) {
      return <p>Loading actors...</p>;
    }
    
    
    const changeHandler = e => {
      setSearch(e.target.value)
    }
    
    //Styled Components
    const Container = styled.div({
      textAlign: 'center'
    });
    const Input = styled.input({
      margin: '40px 0 10px',
      color: 'black'
    })

    return (
      <Fragment>
          <Container>       
             <Input 
               autoFocus 
                type="text" 
                placeholder="Search..."
                value={search}  
                onChange={changeHandler}/>

              <div>               
                {filteredActors.map((data) => (
                  <SearchDetail key={data._id} {...data} />
                ))}         
              </div>

            </Container>
        </Fragment>
    );
};

export default App;